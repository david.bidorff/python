{
  "name": "Python",
  "description": "Build, test and analyse your [Python](https://www.python.org/) projects",
  "template_path": "templates/gitlab-ci-python.yml",
  "kind": "build",
  "prefix": "python",
  "job_prefix": "py",
  "is_component": true,
  "variables": [
    {
      "name": "PYTHON_IMAGE",
      "description": "The Docker image used to run Python - **set the version required by your project**",
      "default": "registry.hub.docker.com/library/python:3-slim"
    },
    {
      "name": "PYTHON_PROJECT_DIR",
      "description": "Python project root directory",
      "default": "."
    },
    {
      "name": "PYTHON_BUILD_SYSTEM",
      "description": "Python build-system to use to install dependencies, build and package the project",
      "type": "enum",
      "values": ["auto", "setuptools", "poetry", "pipenv", "reqfile", "uv"],
      "default": "auto",
      "advanced": true
    },
    {
      "name": "PYTHON_REQS_FILE",
      "description": "Main requirements file _(relative to `$PYTHON_PROJECT_DIR`)_\n\nFor [Requirements Files](https://pip.pypa.io/en/stable/user_guide/#requirements-files) build-system only",
      "default": "requirements.txt",
      "advanced": true
    },
    {
      "name": "PYTHON_EXTRA_REQS_FILES",
      "description": "Extra dev requirements file(s) to install _(relative to `$PYTHON_PROJECT_DIR`)_\n\nFor [Requirements Files](https://pip.pypa.io/en/stable/user_guide/#requirements-files) build-system only",
      "default": "requirements-dev.txt",
      "advanced": true
    },
    {
      "name": "PYTHON_COMPILE_ARGS",
      "description": "[`compileall` CLI options](https://docs.python.org/3/library/compileall.html)",
      "default": "*",
      "advanced": true
    },
    {
      "name": "PIP_OPTS",
      "description": "pip extra [options](https://pip.pypa.io/en/stable/cli/pip/#general-options)",
      "advanced": true
    },
    {
      "name": "PYTHON_EXTRA_DEPS",
      "description": "Extra sets of dependencies to install\n\nFor [Setuptools](https://setuptools.pypa.io/en/latest/userguide/dependency_management.html?highlight=extras#optional-dependencies) or [Poetry](https://python-poetry.org/docs/pyproject/#extras) only"
    }
  ],
  "features": [
    {
      "id":"package",
      "name":"package",
      "description":"This job allows building your Python project [distribution packages](https://packaging.python.org/en/latest/glossary/#term-Distribution-Package).",
      "enable_with": "PYTHON_PACKAGE_ENABLED"
    },
    {
      "id": "pylint",
      "name": "pylint",
      "description": "Code analysis based on [pylint](http://pylint.pycqa.org/en/latest/) Python lib.",
      "enable_with": "PYLINT_ENABLED",
      "variables": [
        {
          "name": "PYLINT_ARGS",
          "description": "Additional [pylint CLI options](http://pylint.pycqa.org/en/latest/user_guide/run.html#command-line-options)",
          "advanced": true
        },
        {
          "name": "PYLINT_FILES",
          "description": "Files or directories to analyse",
          "advanced": true
        }
      ]
    },
    {
      "id": "unittest",
      "name": "unittest",
      "description": "Unit tests based on [unittest](https://docs.python.org/3/library/unittest.html) framework",
      "enable_with": "UNITTEST_ENABLED",
      "variables": [
        {
          "name": "UNITTEST_ARGS",
          "description": "Additional xmlrunner/unittest CLI options",
          "advanced": true
        }
      ]
    },
    {
      "id": "pytest",
      "name": "pytest",
      "description": "Unit tests based on [pytest](https://docs.pytest.org/) framework",
      "enable_with": "PYTEST_ENABLED",
      "variables": [
        {
          "name": "PYTEST_ARGS",
          "description": "Additional [pytest](https://docs.pytest.org/en/stable/usage.html) or [pytest-cov](https://github.com/pytest-dev/pytest-cov#usage) CLI options",
          "advanced": true
        }
      ]
    },
    {
      "id": "nose",
      "name": "nose",
      "description": "Unit tests based on [nose](https://nose.readthedocs.io/) framework",
      "enable_with": "NOSETESTS_ENABLED",
      "variables": [
        {
          "name": "NOSETESTS_ARGS",
          "description": "Additional [nose CLI options](https://nose.readthedocs.io/en/latest/usage.html#options)",
          "advanced": true
        }
      ]
    },
    {
      "id": "bandit",
      "name": "Bandit",
      "description": "[Bandit](https://pypi.org/project/bandit/) analysis (SAST)",
      "enable_with": "BANDIT_ENABLED",
      "variables": [
        {
          "name": "BANDIT_ARGS",
          "description": "Additional [Bandit CLI options](https://github.com/PyCQA/bandit#usage)",
          "default": "--recursive .",
          "advanced": true
        }
      ]
    },
    {
      "id": "trivy",
      "name": "Trivy",
      "description": "Detect security vulnerabilities with [Trivy](https://github.com/aquasecurity/trivy/) (dependencies analysis)",
      "disable_with": "PYTHON_TRIVY_DISABLED",
      "variables": [
        {
          "name": "PYTHON_TRIVY_DIST_URL",
          "description": "Url to the `tar.gz` package for `linux_amd64` of Trivy to use\n\n_When unset, the latest version will be used_",
          "advanced": true
        },
        {
          "name": "PYTHON_TRIVY_ARGS",
          "description": "Additional [Trivy CLI options](https://aquasecurity.github.io/trivy/v0.21.1/getting-started/cli/fs/)",
          "default": "--ignore-unfixed --pkg-types library --detection-priority comprehensive",
          "advanced": true
        }
      ]
    },
    {
      "id": "sbom",
      "name": "Software Bill of Materials",
      "description": "This job generates a file listing all dependencies using [syft](https://github.com/anchore/syft)",
      "disable_with": "PYTHON_SBOM_DISABLED",
      "variables": [
        {
          "name": "PYTHON_SBOM_SYFT_URL",
          "description": "Url to the `tar.gz` package for `linux_amd64` of Syft to use\n\n_When unset, the latest version will be used_",
          "advanced": true
        },
        {
          "name": "PYTHON_SBOM_NAME",
          "description": "Component name of the emitted SBOM",
          "default": "$CI_PROJECT_PATH/$PYTHON_PROJECT_DIR",
          "advanced": true
        },
        {
          "name": "PYTHON_SBOM_OPTS",
          "description": "Options for syft used for SBOM analysis",
          "default": "--override-default-catalogers python-package-cataloger",
          "advanced": true
        }
      ]
    },
    {
      "id": "release",
      "name": "Release",
      "description": "Manually trigger a release of your code (uses [bumpversion](https://pypi.org/project/bumpversion/))",
      "enable_with": "PYTHON_RELEASE_ENABLED",
      "variables": [
        {
          "name": "PYTHON_RELEASE_NEXT",
          "type": "enum",
          "values": [
            "",
            "major",
            "minor",
            "patch"
          ],
          "description": "The part of the version to increase (one of: `major`, `minor`, `patch`)",
          "default": "minor",
          "advanced": true
        },
        {
          "name": "PYTHON_SEMREL_RELEASE_DISABLED",
          "description": "Disable semantic-release integration",
          "type": "boolean",
          "advanced": true
        },
        {
          "name": "PYTHON_RELEASE_COMMIT_MESSAGE",
          "description": "The Git commit message to use on the release commit. This is templated using the [Python Format String Syntax](http://docs.python.org/2/library/string.html#format-string-syntax). Available in the template context are current_version and new_version.",
          "default": "chore(python-release): {current_version} → {new_version}",
          "advanced": true
        },
        {
          "name": "GIT_USERNAME",
          "description": "Git username for Git push operations",
          "secret": true
        },
        {
          "name": "GIT_PASSWORD",
          "description": "Git password for Git push operations",
          "secret": true
        },
        {
          "name": "GIT_PRIVATE_KEY",
          "description": "SSH key for Git push operations",
          "secret": true
        },
        {
          "name": "PYTHON_REPOSITORY_URL",
          "type": "url",
          "description": "Target PyPI repository to publish packages.\n\n_defaults to [GitLab project's packages repository](https://docs.gitlab.com/ee/user/packages/pypi_repository/)_",
          "default": "${CI_SERVER_URL}/api/v4/projects/${CI_PROJECT_ID}/packages/pypi"
        },
        {
          "name": "PYTHON_REPOSITORY_USERNAME",
          "description": "Target PyPI repository username credential",
          "secret": true,
          "default": "gitlab-ci-token"
        },
        {
          "name": "PYTHON_REPOSITORY_PASSWORD",
          "description": "Target PyPI repository password credential",
          "secret": true,
          "default": "$CI_JOB_TOKEN"
        }
      ]
    },
    {
      "id": "black",
      "name": "black",
      "description": "Code formatting based on [black](https://black.readthedocs.io)",
      "enable_with": "PYTHON_BLACK_ENABLED"
    },
    {
      "id": "isort",
      "name": "isort",
      "description": "Check imports order with [isort](https://pycqa.github.io/isort)",
      "enable_with": "PYTHON_ISORT_ENABLED"
    },
    {
      "id": "ruff",
      "name": "Ruff",
      "description": "An extremely fast Python linter and code formatter, written in Rust. [Ruff](https://docs.astral.sh/ruff/)",
      "enable_with": "RUFF_ENABLED",
      "variables": [
        {
          "name": "RUFF_ARGS",
          "description": "Additional [Ruff Linter CLI options](https://docs.astral.sh/ruff/configuration/#full-command-line-interface)",
          "advanced": true
        },
        {
          "name": "RUFF_EXT_EXCLUDE",
          "description": "Define [extend-exclude](https://docs.astral.sh/ruff/settings/#extend-exclude) files",
          "advanced": true
        }
      ]
    },
    {
      "id": "mypy",
      "name": "mypy",
      "description": "Code analysis based on [mypy](https://mypy.readthedocs.io/).",
      "enable_with": "MYPY_ENABLED",
      "variables": [
        {
          "name": "MYPY_ARGS",
          "description": "Additional [mypy CLI options](https://mypy.readthedocs.io/en/stable/command_line.html)",
          "advanced": true
        },
        {
          "name": "MYPY_FILES",
          "description": "Files or directories to analyse",
          "advanced": true
        }
      ]
    }
  ],
  "variants": [
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-python-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest",
          "advanced": true
        },
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url",
          "mandatory": true
        },
        {
          "name": "VAULT_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    },
    {
      "id": "gcp-auth-provider",
      "name": "Google Cloud",
      "description": "Retrieves an [ADC](https://cloud.google.com/docs/authentication/application-default-credentials) for Python Google Client",
      "template_path": "templates/gitlab-ci-python-gcp.yml",
      "variables": [
        {
          "name": "GCP_OIDC_AUD",
          "description": "The `aud` claim for the JWT token _(only required for [OIDC authentication](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/))_",
          "default": "$CI_SERVER_URL",
          "advanced": true
        },
        {
          "name": "GCP_OIDC_ACCOUNT",
          "description": "Default Service Account to which impersonate with OpenID Connect authentication"
        },
        {
          "name": "GCP_OIDC_PROVIDER",
          "description": "Default Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/)"
        }
      ]
    }
  ]
}
